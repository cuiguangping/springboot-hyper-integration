package com.ams.sharding.jdbc.service;

import com.ams.sharding.jdbc.domain.Order;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @author： 乐哥聊编程(全平台同号)
 */
public interface OrderService extends IService<Order> {

}
