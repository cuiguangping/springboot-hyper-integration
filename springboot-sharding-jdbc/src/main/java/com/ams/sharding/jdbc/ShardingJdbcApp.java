package com.ams.sharding.jdbc;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author： 乐哥聊编程(全平台同号)
 */
@SpringBootApplication
@MapperScan("com.ams.sharding.jdbc.mapper")
public class ShardingJdbcApp {
    public static void main(String[] args) {
        SpringApplication.run(ShardingJdbcApp.class, args);
    }
}
