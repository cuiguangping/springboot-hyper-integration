package com.lglbc.springbootkafkacanal;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
/**
 * @author： 乐哥聊编程(全平台同号)
 */
@SpringBootApplication
public class SpringbootKafkaCanalApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootKafkaCanalApplication.class, args);
    }

}
