package com.aims.springbootes.dao;

import com.aims.springbootes.entity.SysUser;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author： 乐哥聊编程(全平台同号)
 */
@Repository
public interface SysUserDao extends ElasticsearchRepository<SysUser,String> {
    Page<SysUser> findEsUserByRolesIn(List<String> roles, Pageable pageable);

}