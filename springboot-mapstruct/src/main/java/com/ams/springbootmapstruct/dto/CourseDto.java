package com.ams.springbootmapstruct.dto;

import lombok.Builder;
import lombok.Data;

/**
 * @author： 乐哥聊编程(全平台同号)
 */
@Data
@Builder
public class CourseDto {
    private String name;
}
