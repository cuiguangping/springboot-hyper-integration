package com.ams.springbootmapstruct.mapper;

import com.ams.springbootmapstruct.dto.StudentDto;
import com.ams.springbootmapstruct.vo.StudentVo;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;
/**
 * @author： 乐哥聊编程(全平台同号)
 */
@Mapper(componentModel = "spring")
public interface MainMapper {
    @Mapping(source = "emailAddress", target = "email")
    StudentDto studentVo2Dto(StudentVo vo);
    List<StudentDto> studentListVo2Dto(List<StudentVo> vo);
}
