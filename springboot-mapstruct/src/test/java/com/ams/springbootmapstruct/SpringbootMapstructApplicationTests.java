package com.ams.springbootmapstruct;

import com.ams.springbootmapstruct.dto.CourseDto;
import com.ams.springbootmapstruct.dto.StudentDto;
import com.ams.springbootmapstruct.mapper.MainMapper;
import com.ams.springbootmapstruct.vo.StudentVo;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@SpringBootTest
class SpringbootMapstructApplicationTests {
    @Autowired
    private MainMapper mainMapper;

    @Test
    void testSimpleMap() {
        List<StudentVo> list = new ArrayList<>();
        StudentVo studentVo = StudentVo.builder()
                .school("清华大学")
                .userId("ams")
                .userName("AI码师")
                .age(27)
                .address("合肥")
                .courseDtos(Arrays.asList(CourseDto.builder().name("语文").build(),CourseDto.builder().name("数学").build()))
                .emailAddress("xxx@qq.com")
                .build();
        list.add(studentVo);
        StudentDto studentDto = mainMapper.studentVo2Dto(studentVo);
        List<StudentDto> studentDtos = mainMapper.studentListVo2Dto(list);
        System.out.println(studentDtos);
    }
}
