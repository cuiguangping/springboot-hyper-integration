package com.lglbc.oauth2.common;

import com.nimbusds.jose.JWSObject;
import com.xiaoleilu.hutool.json.JSONObject;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.text.ParseException;
import java.util.Map;
import java.util.Objects;

/**
 * @author： 乐哥聊编程(全平台同号)
 * @date: 2022/5/29
 */
public class UserContextUtil {
    private static ThreadLocal<Map<String, Object>> threadLocal = new ThreadLocal();
    public static void setUser(Map<String, Object> jsonObject){
        threadLocal.set(jsonObject);
    }

    public static void removeUser(){
        threadLocal.remove();
    }
    public static Long getUserId() {
        Map<String, Object> stringObjectMap = threadLocal.get();
        if (Objects.nonNull(stringObjectMap) && Objects.nonNull(stringObjectMap.get("userId"))){
            return (Long) stringObjectMap.get("userId");
        }
        return -1L;
    }
    public static String getUserName() {
        Map<String, Object> stringObjectMap = threadLocal.get();
        if (Objects.nonNull(stringObjectMap) && Objects.nonNull(stringObjectMap.get("username"))){
            return (String) stringObjectMap.get("username");
        }
        return "";
    }
}
