package com.lglbc.oauth2.entity;

import lombok.Data;
import java.util.Date;
import java.util.List;
import java.io.Serializable;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
/**
 * @description oauth_client
 * @author zhengkai.blog.csdn.net
 * @date 2022-05-28
 */
@Data
public class OauthClient implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.AUTO)
    /**
    * client_id
    */
    private String clientId;

    /**
    * resource_ids
    */
    private String resourceIds;

    /**
    * client_secret
    */
    private String clientSecret;

    /**
    * scope
    */
    private String scope;

    /**
    * authorized_grant_types
    */
    private String authorizedGrantTypes;

    /**
    * web_server_redirect_uri
    */
    private String webServerRedirectUri;

    /**
    * authorities
    */
    private String authorities;

    /**
    * access_token_validity
    */
    private Integer accessTokenValidity;

    /**
    * refresh_token_validity
    */
    private Integer refreshTokenValidity;

    /**
    * additional_information
    */
    private String additionalInformation;

    /**
    * autoapprove
    */
    private String autoapprove;

    /**
    * 创建时间
    */
    private Date createTime;

    /**
    * 更新时间
    */
    private Date updateTime;

    /**
    * 更新者
    */
    private String updateBy;

    /**
    * 创建者
    */
    private String createBy;

    public OauthClient() {}
}