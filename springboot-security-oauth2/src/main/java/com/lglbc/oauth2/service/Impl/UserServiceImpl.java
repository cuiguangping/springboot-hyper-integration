package com.lglbc.oauth2.service.Impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lglbc.oauth2.entity.User;
import com.lglbc.oauth2.mapper.UserMapper;
import com.lglbc.oauth2.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

/**
 * @author： 乐哥聊编程(全平台同号)
 */
@Service
@RequiredArgsConstructor
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {

}
