package com.lglbc.oauth2.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lglbc.oauth2.entity.UserRole;
import org.springframework.stereotype.Service;

/**
 * @description 用户和角色关联表服务层
 * @author zhengkai.blog.csdn.net
 * @date 2022-05-27
 */
@Service
public interface UserRoleService extends IService<UserRole> {



}