package com.lglbc.oauth2.service;

import com.lglbc.oauth2.entity.OauthClient;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.IService;
/**
 * @description oauth_client服务层
 * @author zhengkai.blog.csdn.net
 * @date 2022-05-28
 */
@Service
public interface OauthClientService extends IService<OauthClient> {



}