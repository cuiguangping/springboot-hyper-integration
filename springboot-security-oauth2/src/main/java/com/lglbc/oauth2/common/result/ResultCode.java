package com.lglbc.oauth2.common.result;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author： 乐哥聊编程(全平台同号)
 */
@AllArgsConstructor
@NoArgsConstructor
public enum ResultCode implements IResultCode, Serializable {

    SUCCESS("000000", "成功"),
    SYSTEM_EXECUTION_ERROR("999999", "系统执行出错"),
    AUTH_ERROR("100001", "认证失败"),
    USER_NOT_EXIST("100002", "用户不存在"),
    CLIENT_AUTHENTICATION_FAILED("100003", "客户端认证失败"),
    UNSUPPORTED_GRANT_TYPE_EXCEPTION("100004", "不支持的授权类型"),
    ;

    @Override
    public String getCode() {
        return code;
    }

    @Override
    public String getMsg() {
        return msg;
    }

    private String code;

    private String msg;

    @Override
    public String toString() {
        return "{" +
                "\"code\":\"" + code + '\"' +
                ", \"msg\":\"" + msg + '\"' +
                '}';
    }

}
