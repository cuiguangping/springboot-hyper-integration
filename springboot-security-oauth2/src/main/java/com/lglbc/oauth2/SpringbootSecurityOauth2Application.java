package com.lglbc.oauth2;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootSecurityOauth2Application {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootSecurityOauth2Application.class, args);
    }

}
