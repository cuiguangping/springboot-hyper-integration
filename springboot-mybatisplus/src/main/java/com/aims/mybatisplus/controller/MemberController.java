package com.aims.mybatisplus.controller;

import com.aims.mybatisplus.dao.MemberMapper;
import com.aims.mybatisplus.model.entity.Member;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;

/**
 * @author： 乐哥聊编程(全平台同号)
 */
@Slf4j
@RestController
@RequestMapping("/member")
public class MemberController {
    @Autowired
    private MemberMapper memberMapper;

    @GetMapping
    public String get(@RequestParam("name") String name) {
        LambdaQueryWrapper<Member> lambda = new QueryWrapper<Member>().lambda();
        lambda.eq(Member::getMemberName, name);
        Member member = memberMapper.selectOne(lambda);
        return Objects.isNull(member) ? "没找到信息" : JSON.toJSONString(member);
    }

    @PostMapping
    public int post(@RequestBody Member member) {
        int effectNum = memberMapper.insert(member);
        return effectNum;
    }
}