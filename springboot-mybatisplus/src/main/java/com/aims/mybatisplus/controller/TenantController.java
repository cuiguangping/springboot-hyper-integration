package com.aims.mybatisplus.controller;

import com.aims.mybatisplus.dao.MemberMapper;
import com.aims.mybatisplus.model.entity.Member;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
/**
 * @author： 乐哥聊编程(全平台同号)
 */
@RestController
public class TenantController {
    @Autowired
    private MemberMapper memberMapper;

    @RequestMapping("/testTenant")
    public String testTenantId() {
        Member member = new Member();
        member.setMemberName("测试租户ID");
        memberMapper.insert(member);
        return "success";
    }

    @RequestMapping("/getCurrentTenantMember")
    public List<Member> getCurrentTenantMember() {
        QueryWrapper<Member> queryWrapper = new QueryWrapper<>();
        return memberMapper.selectList(queryWrapper);
    }
}
