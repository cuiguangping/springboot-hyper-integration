package com.lglbc.springbootmq.listener;

import com.lglbc.springbootmq.event.OrderEvent;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

/**
 * @author： 乐哥聊编程(全平台同号)
 * @date: 2022/5/17
 */
@Component
public class GlobalListener {
    @EventListener(OrderEvent.class)
    @Async
    public void createOrder(OrderEvent orderEvent){
        System.out.println("监听器接受消息："+System.currentTimeMillis());
        for (int i=0;i<5;i++){
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("订单创建中.....");
        }
        System.out.println(orderEvent.getMessage());
    }
}
