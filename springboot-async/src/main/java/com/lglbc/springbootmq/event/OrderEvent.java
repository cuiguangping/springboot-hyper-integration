package com.lglbc.springbootmq.event;

import org.springframework.context.ApplicationEvent;

/**
 * @author： 乐哥聊编程(全平台同号)
 * @date: 2022/5/17
 */
public class OrderEvent extends ApplicationEvent {
    private String message;
    public OrderEvent(Object source,String message) {
        super(source);
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
