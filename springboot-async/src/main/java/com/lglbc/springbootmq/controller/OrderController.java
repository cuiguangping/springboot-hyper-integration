package com.lglbc.springbootmq.controller;

import com.lglbc.springbootmq.event.OrderEvent;
import org.springframework.context.ApplicationContext;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author： 乐哥聊编程(全平台同号)
 * @date: 2022/5/17
 */
@RestController
@RequestMapping("/order")
public class OrderController {
    @Resource
    private ApplicationContext applicationContext;
    @RequestMapping("/createOrder/{orderNo}")
    public String createOrder(@PathVariable String orderNo){
        applicationContext.publishEvent(new OrderEvent(this,String.format("订单创建成功，订单号为：%s", orderNo)));
        return "订单正在创建";
    }
}
